import Home from "./pages/Home";

import "./App.css";

function App() {
  return (
    <div className="App">
      <Home />
      <p>Nothing</p>
    </div>
  );
}

export default App;
