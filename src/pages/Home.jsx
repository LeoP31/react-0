import logo from "../assets/logo.svg";
import Counter from "../components/Counter";

export default function Home() {
  return (
    <header className="App-header">
      <img src={logo} className="App-logo" alt="logo" />
      <p>Hello !</p>

      <Counter />

      <p>React est une bibliothèque/un framework</p>
      <p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Lire React
        </a>
        {" |/\\|/\\| "}
        <a
          className="App-link"
          href="https://vitejs.dev/guide/features.html"
          target="_blank"
          rel="noopener noreferrer"
        >
          Documentation Vite
        </a>
      </p>
    </header>
  );
}
